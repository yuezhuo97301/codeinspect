package com.unicom.microservice.mser.mser_zy_demo.dao.impl;

import com.unicom.microservice.mser.mser_zy_demo.dao.TechnologyDao;
import com.unicom.microservice.mser.mser_zy_demo.domain.TechnologyStep;
import com.unicom.microservice.mser.mser_zy_demo.domain.response.Response;
import com.unicom.microservice.mser.mser_zy_demo.repository.TechnologyRepository;
import com.unicom.microservice.mser.mser_zy_demo.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TechnologyDaoImpl implements TechnologyDao {

    @Autowired
    private TechnologyRepository technologyRepository;


    @Override
    public Response<List<TechnologyStep>> getTechnologySteps(int technologyId) {
        List<TechnologyStep> technologySteps = technologyRepository.getTechnologyStepsByTechnologyId(technologyId);
        return ResponseUtil.complete("获取指定工艺下步骤",technologySteps != null,technologySteps);
    }
}
