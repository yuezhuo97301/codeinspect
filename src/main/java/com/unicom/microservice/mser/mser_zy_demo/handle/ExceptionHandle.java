package com.unicom.microservice.mser.mser_zy_demo.handle;

import com.unicom.microservice.mser.mser_zy_demo.domain.response.Response;
import com.unicom.microservice.mser.mser_zy_demo.util.ResponseUtil;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionHandle {

	@ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Response handleException(Exception e){
        return ResponseUtil.complete(e.getMessage(),false,null);
    }
}
