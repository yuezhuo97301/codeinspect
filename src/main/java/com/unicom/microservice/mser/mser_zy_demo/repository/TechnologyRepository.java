package com.unicom.microservice.mser.mser_zy_demo.repository;

import com.unicom.microservice.mser.mser_zy_demo.domain.Technology;
import com.unicom.microservice.mser.mser_zy_demo.domain.TechnologyStep;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TechnologyRepository extends JpaRepository<Technology,Integer> {

    @Query(value = "select ts " +
            "from technology_step ts, technology_related_step trs, technology t " +
            "where (t.id = ?1) and (ts.id = trs.stepId) and (t.id = trs.technologyId) " +
            "order by trs.stepNum asc ")
    List<TechnologyStep> getTechnologyStepsByTechnologyId(int technologyId);
}
