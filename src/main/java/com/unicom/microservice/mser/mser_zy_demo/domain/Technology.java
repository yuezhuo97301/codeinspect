package com.unicom.microservice.mser.mser_zy_demo.domain;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "technology")
public class Technology {

    @JSONField(serialize = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String description;

    @ManyToMany(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @JoinTable(name = "technology_related_step",
            joinColumns = @JoinColumn(name = "technologyId", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "stepId" , referencedColumnName = "id"))
    private Set<TechnologyStep> technologySteps;

    @OneToMany(mappedBy = "technology", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Trades> trades;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<TechnologyStep> getTechnologySteps() {
        return technologySteps;
    }

    public void setTechnologySteps(Set<TechnologyStep> technologySteps) {
        this.technologySteps = technologySteps;
    }
}
