package com.unicom.microservice.mser.mser_zy_demo.repository;

import com.unicom.microservice.mser.mser_zy_demo.domain.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StaffRepository extends JpaRepository<Staff,Integer> {

}
