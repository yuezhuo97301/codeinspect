package com.unicom.microservice.mser.mser_zy_demo.domain.related.unionkey;

import java.io.Serializable;

public class TechnologyRelatedStepKey implements Serializable {

    private int technologyId;

    private int stepId;

    public int getTechnologyId() {
        return technologyId;
    }

    public void setTechnologyId(int technologyId) {
        this.technologyId = technologyId;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }
}
