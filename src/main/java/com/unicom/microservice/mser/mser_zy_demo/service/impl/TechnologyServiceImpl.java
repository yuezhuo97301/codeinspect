package com.unicom.microservice.mser.mser_zy_demo.service.impl;

import com.unicom.microservice.mser.mser_zy_demo.dao.TechnologyDao;
import com.unicom.microservice.mser.mser_zy_demo.domain.TechnologyStep;
import com.unicom.microservice.mser.mser_zy_demo.domain.response.Response;
import com.unicom.microservice.mser.mser_zy_demo.service.TechnologyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TechnologyServiceImpl implements TechnologyService {

    @Autowired
    private TechnologyDao technologyDao;


    @Override
    public Response<List<TechnologyStep>> getTechnologySteps(int technologyId) {
        return technologyDao.getTechnologySteps(technologyId);
    }
}
