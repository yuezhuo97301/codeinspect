package com.unicom.microservice.mser.mser_zy_demo.util;

import java.security.MessageDigest;
import java.util.HashSet;
import java.util.Random;

public class StringUtil {

    public static boolean isStringNotNull(String content){
        if (content != null && !"".equals(content) && !"null".equals(content)){
            return true;
        }
        return false;
    }

    public static String generateCode(){
        String time = TimeUtil.formatStamp2TrimDate(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder(time);
        sb.append(String.format("%04d",new Random().nextInt(9999)));
        return sb.toString();
    }

    public static String string2MD5(String inStr) {
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        char[] charArray = inStr.toCharArray();
        byte[] byteArray = new byte[charArray.length];

        for (int i = 0; i < charArray.length; i++)
            byteArray[i] = (byte) charArray[i];
        byte[] md5Bytes = md5.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++) {
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16)
                hexValue.append("0");
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }

    public static HashSet<String> convertString2Set(String str,String regex) {
        HashSet<String> result = new HashSet<>();
        String []tmps = str.split(regex);
        for (String tmp : tmps){
            result.add(tmp);
        }
        return result;
    }

    public static String generateDuration(long stamp,long step){
        String endTime = TimeUtil.formatStamp2Date(stamp*1000);
        String startTime = TimeUtil.formatStamp2Date(stamp*1000-step*60*1000);
        return "[" + startTime + "-" + endTime + "]";
    }
}
