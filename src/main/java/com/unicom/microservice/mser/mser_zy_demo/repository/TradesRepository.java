package com.unicom.microservice.mser.mser_zy_demo.repository;

import com.unicom.microservice.mser.mser_zy_demo.domain.Trades;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TradesRepository extends JpaRepository<Trades,Integer> {

    @Query(value = "select t " +
            "from trades t " +
            "where (?1 is null or ?1 = '' or t.tradeNumber like %?1%) " +
            "and (?2 is null or ?2 = '' or t.updateTime > CONVERT_TZ(FROM_UNIXTIME(?2), '+00:00','+08:00')) " +
            "and (?3 is null or ?3 = '' or t.updateTime < CONVERT_TZ(FROM_UNIXTIME(?3), '+00:00','+08:00')) ")
    Page<Trades> getTradesInfo(String keyWord, Long startTime, Long endTime, Pageable pageable);
}
