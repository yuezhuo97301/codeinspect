package com.unicom.microservice.mser.mser_zy_demo.util;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class HttpRequestUtil {

    private static Logger logger = LoggerFactory.getLogger(HttpRequestUtil.class);

    public static String httpGetRetryRequest(String url,String key,String value,boolean judgeCode,int timeOutSeconds){
        int retryCount = 3;
        String response = httpGetRequestTimeOut(url,key,value,judgeCode,timeOutSeconds);
        while (response == null && retryCount > 0){
            try {
                logger.info("请求异常后等待3s重试");
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            response = httpGetRequestTimeOut(url,key,value,judgeCode,timeOutSeconds);
            retryCount--;
        }
        return response;
    }

    public static String httpGetRequestTimeOut(String url,String key,String value,boolean judgeCode,int timeOutSeconds) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse httpResponse = null;
        try {
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectionRequestTimeout(1000)                       //设置从connect Manager获取Connection 超时时间
                    .setSocketTimeout(timeOutSeconds*1000)                   //请求获取数据的超时时间
                    .setConnectTimeout(timeOutSeconds*1000)                  //设置连接超时时间
                    .build();
            httpGet.setConfig(requestConfig);
            if (key != null && value != null) {
                httpGet.setHeader(key, value);
            }
            httpResponse = httpClient.execute(httpGet);
            if (judgeCode){
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                if (statusCode < 300 ) {
                    return EntityUtils.toString(httpResponse.getEntity());
                }
            } else {
                return EntityUtils.toString(httpResponse.getEntity());
            }
        } catch (ConnectTimeoutException ex) {
            logger.error("GET请求" + url + "超时:" + ex);
        } catch (Exception e) {
            logger.error("GET请求" + url + "异常:" + e);
        } finally {
            if (httpResponse != null) {
                try {
                    httpResponse.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static String httpPostRequestTimeOut(String url,String key,String value,String requestBody,boolean judgeCode,int timeOutSeconds) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPostRequest = new HttpPost(url);
        CloseableHttpResponse httpResponse = null;
        try {
            httpPostRequest.setHeader("Content-Type", "application/json");
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectionRequestTimeout(1000)                       //设置从connect Manager获取Connection 超时时间
                    .setSocketTimeout(timeOutSeconds*1000)                   //请求获取数据的超时时间
                    .setConnectTimeout(timeOutSeconds*1000)                  //设置连接超时时间
                    .build();
            httpPostRequest.setConfig(requestConfig);
            if(key != null && value != null){
                httpPostRequest.setHeader(key, value);
            }
            if (requestBody != null){
                httpPostRequest.setEntity(new StringEntity(requestBody,"utf-8"));
            }
            httpResponse = httpClient.execute(httpPostRequest);
            if (judgeCode){
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                if (statusCode < 300 ) {
                    return EntityUtils.toString(httpResponse.getEntity());
                }
            } else {
                return EntityUtils.toString(httpResponse.getEntity());
            }
        } catch (ConnectTimeoutException ex) {
            logger.error("POST请求" + url + "超时:" + ex);
        } catch (Exception e) {
            logger.error( "POST请求: " + url + "异常:" + e);
        } finally {
            if (httpResponse != null) {
                try {
                    httpResponse.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static String httpDeleteRequestTimeOut(String url,String key,String value,boolean judgeCode,int timeOutSeconds) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpDelete httpDeleteRequest = new HttpDelete(url);
        CloseableHttpResponse httpResponse = null;
        try {
            httpDeleteRequest.setHeader("Content-Type", "application/json");
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectionRequestTimeout(1000)                       //设置从connect Manager获取Connection 超时时间
                    .setSocketTimeout(timeOutSeconds*1000)                   //请求获取数据的超时时间
                    .setConnectTimeout(timeOutSeconds*1000)                  //设置连接超时时间
                    .build();
            httpDeleteRequest.setConfig(requestConfig);
            if (key != null && value != null) {
                httpDeleteRequest.setHeader(key, value);
            }
            httpResponse = httpClient.execute(httpDeleteRequest);
            if (judgeCode){
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                if (statusCode < 300 ) {
                    return EntityUtils.toString(httpResponse.getEntity());
                }
            } else {
                return EntityUtils.toString(httpResponse.getEntity());
            }
        } catch (ConnectTimeoutException ex) {
            logger.error("DELETE请求" + url + "超时:" + ex);
        } catch (Exception e) {
            logger.error( "DELETE请求: " + url + "异常:" + e);
        } finally {
            if (httpResponse != null) {
                try {
                    httpResponse.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}
