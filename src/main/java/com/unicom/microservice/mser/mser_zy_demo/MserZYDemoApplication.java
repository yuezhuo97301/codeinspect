package com.unicom.microservice.mser.mser_zy_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class MserZYDemoApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MserZYDemoApplication.class, args);
    }
}
