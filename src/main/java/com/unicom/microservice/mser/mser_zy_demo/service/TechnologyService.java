package com.unicom.microservice.mser.mser_zy_demo.service;

import com.unicom.microservice.mser.mser_zy_demo.domain.TechnologyStep;
import com.unicom.microservice.mser.mser_zy_demo.domain.response.Response;

import java.util.List;

public interface TechnologyService {

    Response<List<TechnologyStep>> getTechnologySteps(int technologyId);

}
