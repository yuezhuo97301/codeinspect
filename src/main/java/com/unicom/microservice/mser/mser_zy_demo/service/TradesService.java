package com.unicom.microservice.mser.mser_zy_demo.service;

import com.unicom.microservice.mser.mser_zy_demo.domain.Trades;
import com.unicom.microservice.mser.mser_zy_demo.domain.response.Response;
import org.springframework.data.domain.Page;

public interface TradesService {

    Response<Page<Trades>> getTradesInfo(int page, int size, Long startTime, Long endTime, String keyWord);

}
