package com.unicom.microservice.mser.mser_zy_demo.dao;

import com.unicom.microservice.mser.mser_zy_demo.domain.Trades;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TradesDao {

    Page<Trades> getTradesInfo(String keyWord, Long startTime, Long endTime, Pageable pageable);

}
