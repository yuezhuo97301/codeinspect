package com.unicom.microservice.mser.mser_zy_demo.util;


import com.unicom.microservice.mser.mser_zy_demo.domain.response.Response;

public class ResponseUtil {

    public static <T> Response<T> complete(String msg, boolean flag, T data){
        Response<T> response = new Response<>();
        response.setData(data);
        response.setStatus(flag);
        response.setMessage(msg);
        return response;
    }

}
