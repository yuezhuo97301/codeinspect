package com.unicom.microservice.mser.mser_zy_demo.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {

    public static String formatStamp2Date(long stamp) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(stamp);
        return simpleDateFormat.format(date);
    }

    public static String formatStamp2TrimDate(long stamp) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date(stamp);
        return simpleDateFormat.format(date);
    }

    public static long formatDate2Stamp(String time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static long formatZDate2Stamp(String time) {
        Date date = null;
        try {
            if(time.length() ==29){
                time = time.replace("Z", " UTC");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
                date = sdf.parse(time);
            } else if(time.length() == 24){
                time = time.replace("Z", " UTC");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                date = sdf.parse(time);
            }
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return date.getTime();
    }

    public static boolean compareWithCurrentTime(long stamp){
        return (System.currentTimeMillis()-stamp) < 60000;
    }
}
