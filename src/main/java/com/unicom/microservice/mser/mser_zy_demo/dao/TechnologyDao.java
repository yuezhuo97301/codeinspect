package com.unicom.microservice.mser.mser_zy_demo.dao;

import com.unicom.microservice.mser.mser_zy_demo.domain.TechnologyStep;
import com.unicom.microservice.mser.mser_zy_demo.domain.response.Response;

import java.util.List;

public interface TechnologyDao {

    Response<List<TechnologyStep>> getTechnologySteps(int technologyId);

}
