package com.unicom.microservice.mser.mser_zy_demo.service.impl;

import com.unicom.microservice.mser.mser_zy_demo.dao.TradesDao;
import com.unicom.microservice.mser.mser_zy_demo.domain.Trades;
import com.unicom.microservice.mser.mser_zy_demo.domain.response.Response;
import com.unicom.microservice.mser.mser_zy_demo.service.TradesService;
import com.unicom.microservice.mser.mser_zy_demo.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class TradesServiceImpl implements TradesService {

    @Autowired
    private TradesDao tradesDao;


    @Override
    public Response<Page<Trades>> getTradesInfo(int page, int size, Long startTime, Long endTime, String keyWord) {
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = PageRequest.of(page,size,sort);
        Page<Trades> tradesPage = tradesDao.getTradesInfo(keyWord,startTime,endTime,pageable);
        return ResponseUtil.complete("获取订单信息",tradesPage != null,tradesPage);
    }
}
