package com.unicom.microservice.mser.mser_zy_demo.controller;

import com.unicom.microservice.mser.mser_zy_demo.domain.Trades;
import com.unicom.microservice.mser.mser_zy_demo.domain.response.Response;
import com.unicom.microservice.mser.mser_zy_demo.service.TradesService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/mser/zy/demo")
@RestController
public class TradesController {

    @Autowired
    private TradesService tradesService;

    @GetMapping(value = "/trades/info")
    @ApiOperation(value = "分页查询订单信息")
    public Response<Page<Trades>> getTradesInfo(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam (name = "size", required = false, defaultValue = "10") int size,
            @RequestParam (name = "startTime", required = false) Long startTime,
            @RequestParam (name = "endTime", required = false) Long endTime,
            @RequestParam (name = "keyWord", required = false) String keyWord){
        return tradesService.getTradesInfo(page,size,startTime,endTime,keyWord);
    }
}
