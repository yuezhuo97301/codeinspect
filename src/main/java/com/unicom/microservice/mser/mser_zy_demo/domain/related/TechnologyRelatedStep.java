package com.unicom.microservice.mser.mser_zy_demo.domain.related;

import com.unicom.microservice.mser.mser_zy_demo.domain.related.unionkey.TechnologyRelatedStepKey;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity(name = "technology_related_step")
@IdClass(TechnologyRelatedStepKey.class)
public class TechnologyRelatedStep {

    @Id
    private int technologyId;

    @Id
    private int stepId;

    private int stepNum;

    public int getTechnologyId() {
        return technologyId;
    }

    public void setTechnologyId(int technologyId) {
        this.technologyId = technologyId;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }

    public int getStepNum() {
        return stepNum;
    }

    public void setStepNum(int stepNum) {
        this.stepNum = stepNum;
    }
}
