package com.unicom.microservice.mser.mser_zy_demo.dao.impl;

import com.unicom.microservice.mser.mser_zy_demo.dao.TradesDao;
import com.unicom.microservice.mser.mser_zy_demo.domain.Trades;
import com.unicom.microservice.mser.mser_zy_demo.repository.TradesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class TradesDaoImpl implements TradesDao {

    @Autowired
    private TradesRepository tradesRepository;


    @Override
    public Page<Trades> getTradesInfo(String keyWord, Long startTime, Long endTime,Pageable pageable) {
        return tradesRepository.getTradesInfo(keyWord,startTime,endTime,pageable);
    }
}
