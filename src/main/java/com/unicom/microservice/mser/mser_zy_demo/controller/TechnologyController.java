package com.unicom.microservice.mser.mser_zy_demo.controller;

import com.unicom.microservice.mser.mser_zy_demo.domain.TechnologyStep;
import com.unicom.microservice.mser.mser_zy_demo.domain.response.Response;
import com.unicom.microservice.mser.mser_zy_demo.service.TechnologyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/mser/zy/demo")
@RestController
public class TechnologyController {

    @Autowired
    private TechnologyService technologyService;

    @GetMapping(value = "/technology/steps")
    @ApiOperation(value = "获取指定工艺下的具体步骤")
    public Response<List<TechnologyStep>> getTechnologySteps(
            @RequestParam (name = "technologyId") int technologyId){
        return technologyService.getTechnologySteps(technologyId);
    }
}
